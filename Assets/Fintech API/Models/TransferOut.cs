namespace FintechAPI.Transfers
{
    public class TransferOut
    {
        public string SrcAcctKey;
        public string BenefAcctNo;
        public string BenefName;
        public string PayerName;
        public double Amount;
        public string Ccy;
        public string Nomination;

    }
}
