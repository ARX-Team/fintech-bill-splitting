namespace FintechAPI.Transfers
{
    public class Payment
    {
        public string ServiceId;
        public string UserIdentifier;
        public string CardPan;
        public double Amount;
        public string Currency;
    }
}
