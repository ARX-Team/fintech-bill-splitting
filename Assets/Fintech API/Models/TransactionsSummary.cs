namespace FintechAPI.Transactions
{
    public class TransactionsSummary
    {
        public double OutcomeSum;
        public double IncomeSum;
        public Transaction[] MyOperations;
    }
}