namespace FintechAPI.Clients
{
    [System.Serializable]
    public class UserDetails
    {
        public int UserId;
        public string Username;
        public string Name;
        public string LastName;
        public string PhoneForSms;
        public bool Active;
    }
}