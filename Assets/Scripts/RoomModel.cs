﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoomModel
{

    public float amount;

    public string roomName;
    public List<UserModel> users;
    public int ready;

    public RoomModel(UserModel user, string rn = "Default", float am = 0f)
    {
        roomName = rn;
        amount = am;	
        ready = 0;
        users = new List<UserModel>();
    }


}
