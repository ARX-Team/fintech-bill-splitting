﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{

    #region singleton
    public static UiManager instance;
    private void Awake() { instance = this; }
    #endregion
    public FintechManager fintechManager;
    public FirebaseManager firebaseManager;

    [Header("InputFields")]
    public InputField usernameField;
    public InputField passField;
    public InputField lobbyNameField;
    public InputField moneyNeededField;
    public InputField moneyPledgedAmount;

    public InputField joinLobbyByIdField;




    [Header("Buttons")]

    public Button loginButton;
    public Button createRoomButton;
    public Button joinLobbyButton;

    public Button goToCreateLobbyButton;

    public Button goToJoinLobbyButton;

    [Header("Pages")]

    public GameObject loginpanel;
    public GameObject mainPanel;
    public GameObject createLobbyPanel;
    public GameObject joinLobbyPanel;
    public GameObject lobbyPanel;
    public GameObject addmoneyPanel;

    public GameObject createJoinLobbyPanel;


    public userPrefabController userPrefab;
    public GameObject serPrefab;

    public Transform UserList;



    public Text RequiredAmountText;
    public Text lobbyNameText;
    public Text lobbyIdText;
    public Text lobbyIdText2;


    public Text userAmountText;




    public int pageindex = 0;


    private void Start()
    {
        loginButton.onClick.AddListener(() =>
        {
            fintechManager.Login(usernameField.text, passField.text, (s) =>
            {
                string username = usernameField.text;
                fintechManager.sessionId = s.SessionId;
                fintechManager.GetAccountInfo("sessionId", 0, (a) =>
                  {
                      firebaseManager.currentUser = new UserModel(
                        a.PrintAcctNo, username,
                        (float)a.AvailableAmounts[0].Amount, 0);
                  });
            });
            loginpanel.SetActive(false);
            mainPanel.SetActive(true);
        });

        createRoomButton.onClick.AddListener(() =>
        {
            CreateRoom();
        });

		joinLobbyButton.onClick.AddListener(()=>{

			JoinRoom();

		});

        goToCreateLobbyButton.onClick.AddListener(() =>
        {	
			joinLobbyPanel.SetActive(false);
            createJoinLobbyPanel.SetActive(true);
            createLobbyPanel.SetActive(true);
        });
        goToJoinLobbyButton.onClick.AddListener(() =>
        {
			createLobbyPanel.SetActive(false);
            createJoinLobbyPanel.SetActive(true);
            joinLobbyPanel.SetActive(true);
        });


    }


    public void CreateRoom()
    {
        RoomModel rm = new RoomModel(firebaseManager.currentUser, lobbyNameField.text, float.Parse(moneyNeededField.text));
        firebaseManager.CreateRoom(rm, lobbyNameField.text);
        firebaseManager.GetRoomData(lobbyNameField.text, () =>
        {
            FillUsers(firebaseManager.roomdata.users);
            InputLobbyInfo();

        });
        //	FillUsers(firebaseManager.roomdata.users);
    }

    public void JoinRoom()
    {	
		Debug.Log(joinLobbyByIdField.text);
		firebaseManager.JoinRoom(joinLobbyByIdField.text,firebaseManager.currentUser);

		firebaseManager.GetRoomData(joinLobbyByIdField.text, () =>
        {
            FillUsers(firebaseManager.roomdata.users);
            InputLobbyInfo();

        });
    }

    void InputLobbyInfo()
    {
        lobbyIdText.text = firebaseManager.roomId;
        lobbyIdText2.text = firebaseManager.roomId;
        lobbyNameText.text = firebaseManager.roomdata.roomName;
        RequiredAmountText.text = firebaseManager.roomdata.amount + "";
        userAmountText.text = firebaseManager.roomdata.users.Count + "";
    }

    public void FillUsers(List<UserModel> users)
    {

        foreach (Transform child in UserList)
        {
            Destroy(child.gameObject);
        }
        Debug.Log(users.Count);
        lobbyPanel.SetActive(true);
        foreach (UserModel um in users)
        {
            userPrefabController upm = Instantiate(userPrefab, UserList);
            upm.nameText.text = um.userName;
            upm.moneyAmount.text = um.pledgedAmount + "";
        }
    }
}
