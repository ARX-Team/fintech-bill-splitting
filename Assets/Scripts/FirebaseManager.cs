﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;

public class FirebaseManager : MonoBehaviour
{
    #region singleton
    public static FirebaseManager instance;
    private void Awake() {instance = this;}
    #endregion
    public DatabaseReference dbref;
    public UserModel currentUser;
    public RoomModel rm;

    public RoomModel roomdata;
    [SerializeField]
    public UserModel user;

    public int myId;

    public string roomId;






    private void Start()
    {
         FirebaseApp app = FirebaseApp.DefaultInstance;
        // app.SetEditorDatabaseUrl("https://fintech-grouppay.firebaseio.com/");
        // if (app.Options.DatabaseUrl != null)
        //     app.SetEditorDatabaseUrl(app.Options.DatabaseUrl);

        FirebaseDatabase database = FirebaseDatabase.DefaultInstance;
        DatabaseReference myRef = database.RootReference;
        DatabaseReference refer = database.GetReference("");
        Debug.Log(myRef);
        Debug.Log(refer);
        dbref = FirebaseDatabase.DefaultInstance.RootReference;
        Debug.Log(dbref);
        // rm = new RoomModel(currentUser);

        //roomId = CreateRoom(rm);
    }

    void HandleUserDataChange(object sender, ChildChangedEventArgs args)
    {
        Debug.Log("user data change");
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        user = JsonUtility.FromJson<UserModel>(args.Snapshot.GetRawJsonValue());
        // Do something with the data in args.Snapshot
    }
    void HandleNewUser(object sender, ChildChangedEventArgs args)
    {
        Debug.Log("new user");
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        user = JsonUtility.FromJson<UserModel>(args.Snapshot.GetRawJsonValue());

        GetRoomData(roomId,()=>{ UiManager.instance.FillUsers(roomdata.users);});
        // Do something with the data in args.Snapshot
    }

    void HandleUserLeave(object sender, ChildChangedEventArgs args)
    {
        Debug.Log("user left");
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        user = JsonUtility.FromJson<UserModel>(args.Snapshot.GetRawJsonValue());
        // Do something with the data in args.Snapshot
    }
     void HandleReadyState(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        Debug.Log("flag changed");
        int value = System.Convert.ToInt32(args.Snapshot.Value);
        if(value>0)
        Debug.Log("READY");
        else
        Debug.Log("NOT READY");
    }
    public void CreateRoom(RoomModel room,string rid)
    {   
        Debug.Log(room.ready + " "+ rid +" ");
        roomId = rid;
        Debug.Log(roomId);
        Debug.Log(dbref);
        dbref.Child(rid).SetRawJsonValueAsync(JsonUtility.ToJson(room)).
        ContinueWith((r)=>
        {
            if(r.IsFaulted||r.IsCanceled)
                Debug.Log("ERROR");
            else
                JoinRoom(rid, currentUser);

        });
        Debug.Log(JsonUtility.ToJson(room));
        Debug.Log(currentUser.userName);
    }

    public void GetRoomData(string roomid = "123", System.Action a = null)
    {
        dbref.Child(roomid).GetValueAsync().ContinueWith((t) =>
        {
            if (t.IsFaulted || t.IsCanceled)
            {
                Debug.Log("failed");
            }
            else
            {
                roomdata = JsonUtility.FromJson<RoomModel>(t.Result.GetRawJsonValue());
                Debug.Log(roomdata.users.Count);
                a.Invoke();
            }
        });
    }

    public void ToggleReady()
    {
        dbref.Child(roomId).Child("ready").RunTransaction(mutableData=>{
            int value = System.Convert.ToInt32(mutableData.Value);
            mutableData.Value =  Mathf.Abs(value-1);
            Debug.LogWarning(value);
            return TransactionResult.Success(mutableData);
        });
    }

    public void JoinRoom(string roomid, UserModel um)
    {

        dbref.Child(roomid).Child("users").RunTransaction(mutableData =>
        {

            List<object> users = mutableData.Value as List<object>;
            if (users == null)
            {
                users = new List<object>();
                myId = 0;
            }
            else
                myId = (int)mutableData.ChildrenCount;

            Dictionary<string, object> newuser = new Dictionary<string, object>();
            newuser["accountNum"] = um.accountNum;
            newuser["availableAmount"] = um.availableAmount;
            newuser["pledgedAmount"] = um.pledgedAmount;
            newuser["userName"] = um.userName;
            users.Add(newuser);
            mutableData.Value = users;
            return TransactionResult.Success(mutableData);
        });
        dbref.Child(roomId).Child("users").ChildAdded += HandleNewUser;
        dbref.Child(roomId).Child("users").ChildRemoved += HandleUserLeave;
        dbref.Child(roomId).Child("users").ChildChanged += HandleUserDataChange;
        dbref.Child(roomId).Child("ready").ValueChanged += HandleReadyState;
    }



    //.SetRawJsonValueAsync(JsonUtility.ToJson(um));



    public void LeaveRoom()
    {
        dbref.Child(roomId).Child("users").Child(myId + "").RemoveValueAsync();
    }




    private void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 100, 30), "Join"))
            JoinRoom(roomId, currentUser);
        if (GUI.Button(new Rect(0, 30, 100, 30), "Leave"))
            LeaveRoom();
        // if (GUI.Button(new Rect(0, 60, 100, 30), "Create"))
        //     CreateRoom(rm,123);
        // if (GUI.Button(new Rect(0, 90, 100, 30), "get room data"))
        //     GetRoomData(roomId);
        // if(GUI.Button(new Rect(0,120,100,30),"trigger ready"))
        //     ToggleReady();
        // if(GUI.Button(new Rect(0,150,100,30),"Leave room"))
        //     LeaveRoom();

    }




}

