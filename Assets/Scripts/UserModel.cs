﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserModel
{

    public string accountNum;

    public float availableAmount;

    public float pledgedAmount;

    public string userName;

    public UserModel(string acnum,string usnam, float avam, float pledgam)
    {
        accountNum = acnum;
        availableAmount = avam;
        pledgedAmount = pledgam;
        userName = usnam;
    }

}
